﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class k_EnemyWeapon : MonoBehaviour
{
    //Attach this script to the weapon, this object collider will active when the attack animation played 
    public int attackDamage = 10;
    private AudioSource audio;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            audio.Play();
            other.GetComponent<PlayerStats>().DecreaseHealth(attackDamage);
        }
    }
}
