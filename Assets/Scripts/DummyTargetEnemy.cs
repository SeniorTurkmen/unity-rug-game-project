﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyTargetEnemy : MonoBehaviour
{
    public bool isTargetable = true;

    public bool GetTargetable()
    {
        return isTargetable;
    }
}
